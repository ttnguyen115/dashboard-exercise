import axiosClient from "./axiosClient";

export const userApi = {
    getAll: (page) => {
        const url = '/QuanLyNguoiDung/LayDanhSachNguoiDungPhanTrang';
        return axiosClient.get(url, {
            params: {
                maNhom: "GP10",
                soTrang: page
            }
        })
    },

    editUser: ({ token, payload }) => {
        const url = '/QuanLyNguoiDung/CapNhatThongTinNguoiDung';
        return axiosClient.post(url, {
            "taiKhoan": payload.username,
            "matKhau": payload.password,
            "email": payload.email,
            "soDt": payload.phone,
            "maNhom": "GP10",
            "maLoaiNguoiDung": payload.userType,
            "hoTen": payload.fullname
        }, {
            headers: { Authorization: 'Bearer ' + token }
        });
    },

    addUser: ({ token, payload }) => {
        const url = '/QuanLyNguoiDung/ThemNguoiDung';
        return axiosClient.post(url, {
            "taiKhoan": payload.username,
            "matKhau": payload.password,
            "email": payload.email,
            "soDt": payload.phone,
            "maNhom": "GP10",
            "maLoaiNguoiDung": payload.userType,
            "hoTen": payload.fullname
        }, {
            headers: { Authorization: 'Bearer ' + token }
        });
    },

    deleteUser: ({ token, payload }) => {
        const url = '/QuanLyNguoiDung/XoaNguoiDung';
        return axiosClient.delete(url, {
            headers: { Authorization: 'Bearer ' + token },
            params: payload,
        });
    }
};
