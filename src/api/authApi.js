import axiosClient from "./axiosClient";

export const authApi = {
    login: ({ username, password }) => {
        const url = '/QuanLyNguoiDung/DangNhap';
        return axiosClient.post(url, {
            'taiKhoan': username,
            'matKhau': password
        });
    },

    refreshToken: (token) => {
        const url = "/QuanLyNguoiDung/ThongTinTaiKhoan";
        return axiosClient.post(url, token, {
            headers: { Authorization: "Bearer " + token }
        });
    }
};
