import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    currentUser: null,
}

const authReducer = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        refresh: (state, { payload }) => {
            state.currentUser = payload;
        },

        login: (state, { payload }) => {
            localStorage.setItem('token', payload.accessToken);
            state.currentUser = payload;
        },

        logout: (state) => {
            localStorage.removeItem('token');
            state.currentUser = null;
        }
    }
});

export const { login, logout, refresh } = authReducer.actions;

export const authSelector = state => state;

export default authReducer.reducer;