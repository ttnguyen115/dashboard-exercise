import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import auth from './authSlice';
import users from './userSlice';
import rootSaga from './saga';

const sagaMiddleware = createSagaMiddleware();
export default configureStore({
    reducer: {
        auth,
        users,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware),
});
sagaMiddleware.run(rootSaga);