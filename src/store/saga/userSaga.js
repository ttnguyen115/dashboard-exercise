import { call, put, takeLatest } from 'redux-saga/effects';
import { userApi } from '../../api/userApi';
import { getUsers } from '../userSlice';
import { sagaTypes } from './sagaTypes';

function* handleFetchUsers() {
    try {
        const res = yield call(userApi.getAll, 1);
        yield put(getUsers(res));
    } catch (err) {
        console.log(err);
    }
}

function* handleAddUser({ payload }) {
    try {
        const token = localStorage.getItem('token');
        yield call(userApi.addUser, { token, payload });
        const res = yield call(userApi.getAll, 1);
        yield put(getUsers(res));
    } catch (err) {
        console.log(err);
    }
}

function* handleEditUser({ payload }) {
    try {
        const token = localStorage.getItem('token');
        yield call(userApi.editUser, { token, payload });
        const res = yield call(userApi.getAll, 1);
        yield put(getUsers(res));
    } catch (err) {
        console.log(err);
    }
}

function* handleDeleteUser({ payload }) {
    try {
        const token = localStorage.getItem('token');
        yield call(userApi.deleteUser, { token, payload });
        const res = yield call(userApi.getAll, 1);
        yield put(getUsers(res));
    } catch (err) {
        console.log(err);
    }
}

export function* userSaga() {
    yield takeLatest(sagaTypes.FETCH_USERS, handleFetchUsers);
    yield takeLatest(sagaTypes.ADD_USER, handleAddUser);
    yield takeLatest(sagaTypes.EDIT_USER, handleEditUser);
    yield takeLatest(sagaTypes.DELETE_USER, handleDeleteUser);
}