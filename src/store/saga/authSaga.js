import { call, fork, put, take, takeLatest } from 'redux-saga/effects';
import { authApi } from '../../api/authApi';
import { login, logout, refresh } from '../authSlice';
import { clearUsers } from '../userSlice';
import { sagaTypes } from './sagaTypes';

function* handleToken() {
    const token = localStorage.getItem('token');
    try {
        if (token) {
            const res = yield call(authApi.refreshToken, token);
            yield put(refresh({ ...res, token }));
        }
    } catch (err) {
        console.log(err)
    }
}

function* handleLogin({ data, handleRedirect }) {
    try {
        const res = yield call(authApi.login, data);
        if (res.maLoaiNguoiDung === 'QuanTri') {
            yield put(login(res));
            handleRedirect();
        }
        else console.log("You are not allowed to access!");
    } catch (err) {
        console.log(err);
    }
}

function* handleLogout() {
    yield put(logout());
    yield put(clearUsers());
}

function* watchAuthFlow() {
    while(true) {
        const token = localStorage.getItem('token');
        if (!token) {
            const action = yield take(sagaTypes.LOGIN);
            yield fork(handleLogin, action.payload);
        }
    
        yield take(sagaTypes.LOGOUT);
        yield call(handleLogout);
    }
}

export function* authSaga() {
    yield fork(watchAuthFlow);
    yield takeLatest(sagaTypes.FETCH_TOKEN, handleToken);
}