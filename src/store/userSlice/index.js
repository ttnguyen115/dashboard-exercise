import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    users: null,
    selectedUser: null,
    openModal: false,
    formStatus: null,
}

const userReducer = createSlice({
    name: 'users',
    initialState,
    reducers: {
        getUsers: (state, { payload }) => {
            state.users = payload;
        },

        setSelectedUser: (state, { payload }) => {
            state.selectedUser = payload.selectedUser;
            state.openModal = payload.openModal;
            state.formStatus = payload.formStatus;
        },

        clearUsers: state => {
            state.users = null;
        }
    }
});

export const { getUsers, clearUsers, setSelectedUser } = userReducer.actions;

export const userSelector = state => state;

export default userReducer.reducer;