import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Switch } from "react-router-dom";
import { AuthRoute, PrivateRoute } from './HOCs/Routes';
import { createAction } from "./store/createAction";
import { sagaTypes } from "./store/saga/sagaTypes";
import Home from './views/Home';
import Login from "./views/Login";

const App = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(createAction(sagaTypes.FETCH_TOKEN));
    }, [dispatch]);

    return (
        <Switch>
            <AuthRoute path="/login" component={Login} redirectPath="/" />
            <PrivateRoute path="/" component={Home} redirectPath="/login" />
        </Switch>
    );
  }
  
export default App;
  