import { AppBar, Button, Toolbar, Typography } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { createAction } from '../../store/createAction';
import { sagaTypes } from '../../store/saga/sagaTypes';

const Header = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { currentUser } = useSelector(state => state.auth);

    const handleLogout = () => {
        dispatch(createAction(sagaTypes.LOGOUT));
        history.push('/login');
    }

    return (
        <AppBar position="static">
            <Toolbar style={{ display: 'flex', justifyContent: 'space-between' }}>
                {/* <IconButton edge="start" color="inherit" aria-label="menu">
                    <MenuIcon />
                </IconButton> */}
                <Typography variant="h6">
                    Hello, {currentUser?.taiKhoan}
                </Typography>
                <Button color="inherit" onClick={handleLogout}><ExitToAppIcon /></Button>
            </Toolbar>
        </AppBar>
    )
}

export default Header
