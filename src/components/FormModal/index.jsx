import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Dialog, DialogContent, DialogTitle } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from "yup";
import InputField from '../FormControl/InputField';
import PasswordField from '../FormControl/PasswordField';
import RadioField from '../FormControl/RadioField';

const schema = yup.object().shape({
    username: yup.string()
        .required('Please enter username.'),
    fullname: yup.string()
        .required('Please enter full name.')
        .test('Should has at least two words.','Please enter at least two words.', value => {
            return value.split(' ').length >= 2
        }),
    email: yup.string()
        .required('Please enter email.')
        .email('Please enter an valid email address.'),
    phone: yup.string()
        .required('Please enter phone.'),
    password: yup.string()
        .required('Please enter password.')
        .min(6,'Please enter at least six characters.'),
});

const FormModal = ({ open, handleClose, handleSubmitValues, selectedUser }) => {
    const { setValue, control, handleSubmit, reset, formState: { isSubmitting, isSubmitSuccessful } } = useForm({
        defaultValues: {
            username: "",
            email: "",
            phone: "",
            fullname: "",
            password: "",
            userType: "KhachHang",
        },
        reValidateMode: 'onSubmit',
        resolver: yupResolver(schema)
    });

    useEffect(() => {
        if (selectedUser) {
            setValue("username", selectedUser.taiKhoan);
            setValue("email", selectedUser.email);
            setValue("phone", selectedUser.soDt);
            setValue("fullname", selectedUser.hoTen);
            setValue("password", selectedUser.matKhau);
            setValue("userType", selectedUser.maLoaiNguoiDung);
        }

        if (isSubmitSuccessful) {
            handleClose();
            reset();
        }
    }, [isSubmitSuccessful, handleClose, reset, selectedUser, setValue])
    
    const onSubmit = data => handleSubmitValues(data);

    return (
        <>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{!selectedUser ? 'Add' : 'Edit'} Form</DialogTitle>
                <DialogContent>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <InputField 
                            control={control} 
                            name="username" 
                            label="Username" 
                        />

                        <InputField 
                            control={control} 
                            name="email"
                            label="Email"
                        />

                        <InputField 
                            control={control} 
                            name="fullname" 
                            label="Full Name"
                        />

                        <InputField 
                            control={control} 
                            name="phone" 
                            label="Phone"
                        />

                        <PasswordField 
                            control={control} 
                            name="password" 
                            label="Password"
                        />

                        <RadioField 
                            control={control} 
                            name="userType" 
                            label="User Type" 
                        />


                        <Button
                            disabled={isSubmitting}
                            type="submit"
                            variant="contained"
                            color="primary"
                            size="large"
                        >
                            {!selectedUser ? 'Add' : 'Edit'}
                        </Button>
                        <Button 
                            disabled={isSubmitting}
                            onClick={handleClose} 
                            color="primary" 
                            type="button"
                            size="large"
                            variant="outlined"
                        >
                            Cancel
                        </Button>
                    </form>
                </DialogContent>
            </Dialog>
        </>
    )
}

export default FormModal
