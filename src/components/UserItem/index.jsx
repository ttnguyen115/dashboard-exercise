import { IconButton, TableCell, TableRow } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { createAction } from '../../store/createAction';
import { sagaTypes } from '../../store/saga/sagaTypes';
import { setSelectedUser } from '../../store/userSlice';

const UserItem = ({ user }) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { taiKhoan, email, soDt, hoTen } = user;
    const handleClickOpen = () => dispatch(setSelectedUser({ openModal: true, selectedUser: user, formStatus: 'Edit' }));
    const handleDelete = () => {
        dispatch(createAction(sagaTypes.DELETE_USER, { taiKhoan }));
        history.push("/login")
    }

    return (
        <>
            <TableRow>
                <TableCell component="th" scope="row">{taiKhoan}</TableCell>
                <TableCell>{hoTen}</TableCell>
                <TableCell>{email}</TableCell>
                <TableCell>{soDt}</TableCell>
                <TableCell align="right">
                    <IconButton edge="end" aria-label="edit" onClick={handleClickOpen}><EditIcon /></IconButton>
                    <IconButton edge="end" aria-label="delete" onClick={handleDelete}><DeleteIcon /></IconButton>
                </TableCell>
            </TableRow>
        </>
    )
}

export default UserItem
