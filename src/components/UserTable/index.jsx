import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { userApi } from '../../api/userApi';
import { getUsers } from '../../store/userSlice';
import UserItem from '../UserItem';

const UserTable = () => {
    const dispatch = useDispatch();
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const [userList, setUserList] = useState([]);

    useEffect(() => {
        const fetchUserTable = async (page) => {
            try {
                const { totalPages, items } = await userApi.getAll(page);
                dispatch(getUsers(items));
                setTotalPages(totalPages);
                setUserList(items);
            } catch (err) {
                console.log(err);
            }
        } 
        fetchUserTable(page);
    }, [page, dispatch]);

    const handlePageChange = (e, page) => setPage(page);

    return (
        <TableContainer component={Paper}>
            <Table aria-label="users table">
                <TableHead>
                    <TableRow>
                        <TableCell>Username</TableCell>
                        <TableCell>Full Name</TableCell>
                        <TableCell>Email</TableCell>
                        <TableCell>Phone</TableCell>
                        <TableCell align="right">Action</TableCell>
                    </TableRow>
                </TableHead>

                <TableBody>
                    {
                        userList?.map((user, index) => (
                            <UserItem user={user} key={index} />
                        ))
                    }
                </TableBody>
            </Table>

            <Pagination count={totalPages} color="primary" onChange={handlePageChange} />
        </TableContainer>
    )
}

export default UserTable
