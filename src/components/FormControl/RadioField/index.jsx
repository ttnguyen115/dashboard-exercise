import { RadioGroup, FormControlLabel, Radio } from '@material-ui/core'
import React from 'react'
import { Controller } from 'react-hook-form'

const RadioField = ({ name, control, label }) => {
    return (
        <Controller
            name={name}
            control={control}
            render={({ 
                field: { onChange, value, name }, 
                fieldState: { invalid, error } 
            }) => (
                <RadioGroup 
                    aria-label={label} 
                    name={name} 
                    value={value} 
                    onChange={onChange} 
                >
                    <FormControlLabel value="KhachHang" control={<Radio color="primary" />} label="Khach Hang" />
                    <FormControlLabel value="QuanTri" control={<Radio color="primary" />} label="Quan Tri" />
                </RadioGroup>
            )}
        />
    )
}

export default RadioField
