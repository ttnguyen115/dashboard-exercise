import { FormControl, FormHelperText, IconButton, InputAdornment, InputLabel, OutlinedInput } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import React, { useState } from 'react';
import { Controller } from 'react-hook-form';

const PasswordField = ({ control, name, label }) => {
    const [showPassword,setShowPassword] = useState(false);
    const toggleShowPassword = () => setShowPassword(!showPassword);

    return (
        <Controller
            name={name}
            control={control}
            render={
                ({ 
                    field: { onChange, onBlur, value, name },
                    fieldState: { error, invalid, isTouched }
                }) => (
                    <>
                        <FormControl 
                            margin="normal"
                            error={isTouched && invalid} 
                            fullWidth 
                            variant="outlined"
                        >
                            <InputLabel>{label}</InputLabel>
                            <OutlinedInput
                                id={name}
                                error={invalid}
                                type={showPassword ? 'text' : 'password'}
                                label={label}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={toggleShowPassword}
                                            edge="end"
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                labelWidth={70}
                                value={value}
                                onBlur={onBlur}
                                onChange={onChange}
                            />
                        </FormControl>
                        <FormHelperText error={invalid}>{error?.message}</FormHelperText>
                    </>
                )
            }
        />
    )
}

export default PasswordField
