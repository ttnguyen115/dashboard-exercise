import { Button, Container } from '@material-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import FormModal from '../../components/FormModal';
import Header from '../../components/Header';
import UserTable from '../../components/UserTable';
import { createAction } from '../../store/createAction';
import { sagaTypes } from '../../store/saga/sagaTypes';
import { setSelectedUser } from '../../store/userSlice';

const Home = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { openModal, selectedUser, formStatus } = useSelector(state => state.users);
    const handleClickOpen = () => dispatch(setSelectedUser({ openModal: true, selectedUser: null, formStatus: 'Add' }));
    const handleClose = () => dispatch(setSelectedUser({ openModal: false, selectedUser: null, formStatus: null }));
    const handleSubmitValues = data => {
        if (formStatus === 'Add') dispatch(createAction(sagaTypes.ADD_USER, data));
        else if (formStatus === 'Edit') dispatch(createAction(sagaTypes.EDIT_USER, data));
        else dispatch(setSelectedUser({ openModal: false, selectedUser: null, formStatus: null }));
        dispatch(setSelectedUser({ openModal: false, selectedUser: null, formStatus: null }));
        history.push("/login");
    }

    return (
        <>
            <Header />
            <Container maxWidth="lg">
                <Button variant="outlined" color="primary" onClick={handleClickOpen}>Add new</Button>
                <FormModal open={openModal} handleClose={handleClose} handleSubmitValues={handleSubmitValues} selectedUser={selectedUser} />
                <UserTable />
            </Container>
        </>
    )
}

export default Home
