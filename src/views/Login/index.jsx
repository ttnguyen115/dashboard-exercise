import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Container, Paper, Typography } from '@material-ui/core';
import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import * as yup from "yup";
import InputField from '../../components/FormControl/InputField';
import PasswordField from '../../components/FormControl/PasswordField';
import { createAction } from '../../store/createAction';
import { sagaTypes } from './../../store/saga/sagaTypes';

const schema = yup.object().shape({
    username: yup.string()
        .required('Please enter your username.'),
    password: yup.string()
        .required('Please enter your password.')
        .min(6,'Please enter at least six characters.'),
});

const Login = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { control, handleSubmit, formState: { isSubmitting } } = useForm({
        defaultValues: {
            username: '',
            password: '',
        },
        reValidateMode: 'onSubmit',
        resolver: yupResolver(schema)
    });

    const onSubmit = data => {
        const handleRedirect = () => history.push("/");
        dispatch(createAction(sagaTypes.LOGIN, { data, handleRedirect }));
    }

    return (
        <Container maxWidth="sm" style={{ height: '100vh', display: 'grid', placeItems: 'center' }}>
            <Paper variant="outlined" style={{ padding: 40, borderRadius: 10 }}>
                <Typography variant="h5" component="h6">Log in</Typography>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <InputField control={control} name="username" label="Username"/>
                    <PasswordField control={control} name="password" label="Password"/>
                    <Button
                        disabled={isSubmitting}
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        size="large"
                    >
                        Log in
                    </Button>
                </form>
            </Paper>
        </Container>
    )
}

export default Login
